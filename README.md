# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* forum
* Key functions (add/delete)
    1. post page
    2. submit page
    3. [xxx]
    4. [xxx]
* Other functions (add/delete)
    1. verify password (length)
    2. sign in with facebook amd google
    3. RWD 
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
1. 在"貼文"頁面可以看到所有的貼文，並可進行留言
2. 在"發文"頁面則可以發布貼文

## Security Report (Optional)
