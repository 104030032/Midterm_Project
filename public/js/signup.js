function initApp() {
    //
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var repaetPassword = document.getElementById('repeatPassword');
    var username = document.getElementById('username');
    var btnSignUp = document.getElementById('btnSignUp');
    //
    btnSignUp.addEventListener('click', function () {
        var password=txtPassword.value;
        var password_re=repaetPassword.value;
        try { 
            if(password !== password_re) throw "password doesn't match";
            if(password.length<8) throw "password too short,at least 8 chacacters"
        }
        catch(err) {
            alert(err);
            txtEmail.value='';
            username.value='';
            txtPassword.value='';
            repaetPassword.value='';
        }           
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value)
            .then(function(){
                alert("Register Success!!");
                window.location.href = "signin.html";
            })
            .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            txtEmail.value='';
            username.value='';
            txtPassword.value='';
            repaetPassword.value='';
            });                      

    });
}


window.onload = function () {
    initApp();
};