function init() {
    //for logout
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('to_add');
        var cancel = document.getElementById('to_cancel');
        var cancel2 = document.getElementById('cancel');
        if (user) {
            user_email = user.email;
            cancel.innerHTML='';
            cancel2.innerHTML='';
            menu.innerHTML = "<a class='nav-link' id='logout-btn'>Logout</a>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "";
            cancel2.innerHTML='<h1 class="cover-heading"><a href="signin.html">Sign in</a> to continue</h1>';
            cancel.innerHTML='<a class="nav-link"  href="signin.html">登入</a>';
            document.getElementById('post_list').innerHTML = "";
        }
    });
    //post
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('post_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
    }).then(function(){window.location = "post.html";}).catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}