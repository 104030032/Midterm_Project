function initApp() {

    var Email = document.getElementById('Email');
    var Password = document.getElementById('Password');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btnGoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    

    btnLogin.addEventListener('click', function () {
        var email = Email.value;
        var password = Password.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(function(){
                alert("welcome");
                window.location.href = "index.html";
            }).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log('error: ' + error.message);
            }); 

    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location = "index.html";
        }).catch(function (error) {
            console.log('error: ' + error.message);
        });

    });

    btnFacebook.addEventListener('click',function(){        
        var provider_f = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider_f).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location = "index.html";
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};