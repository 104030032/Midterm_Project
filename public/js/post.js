function init() {
    //for logout
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('to_add');
        var cancel = document.getElementById('to_cancel');
        var cancel2 = document.getElementById('cancel');
        if (user) {
            user_email = user.email;
            cancel.innerHTML='';
            cancel2.innerHTML='';
            menu.innerHTML = "<a class='nav-link' id='logout-btn'>Logout</a>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "";
            cancel2.innerHTML='<h1 class="cover-heading"><a href="signin.html">Sign in</a> to continue</h1>';
            cancel.innerHTML='<a class="nav-link"  href="signin.html">登入</a>';
            document.getElementById('post_list').innerHTML = "";
        }
    });


    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n<div class='my-3 p-3 bg-gray rounded box-shadow'><h5 class='border-bottom border-gray pb-2 mb-0'>Comment</h5><textarea class='form-control' rows='1' id='comment' style='resize:none'></textarea><div class='media text-muted pt-3'><button id='comment_btn' type='button' class='btn btn-success'>Submit</button></div></div>";

    //for post
    var postsRef = firebase.database().ref('post_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
            }})
            //add comment
            var comment_btn = document.getElementById('comment_btn');
            var text = document.getElementById('comment');

            comment_btn.addEventListener('click',function(){
                var commentRef = firebase.database().ref("post_list/"+data.key+"/comments").push();
                commentRef.set({
                    comment:text.value,
                    email: user.email,
                });
                text.value = "";
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}