function init() {
    //for logout
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('to_add');
        var cancel = document.getElementById('to_cancel');
        var cancel2 = document.getElementById('cancel');
        if (user) {
            user_email = user.email;
            cancel.innerHTML="";
            cancel2.innerHTML='';
            menu.innerHTML = "<a class='nav-link' id='logout-btn'>Logout</a>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "";
            cancel.innerHTML='<a class="nav-link"  href="signin.html">登入</a>';
        }
    });
}

window.onload = function () {
    init();
}